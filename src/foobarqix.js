const foobarqixObj = {
    3: 'Foo',
    5: 'Bar',
    7: 'Qix'
}

function foobarqix(number) {
    // create some variable to store the string results
    let results="";
    // check if number is divisible by foo, bar, or qix numbers, and add to the string results accordingly
   results += constructFooBarQixString(number, divisibleByDivisor);
    // check if number contains foo, bar, or qix numbers, and all to the string results accordingly
    results += constructFooBarQixString(number, containsFooBarNumber);
    // return string results
    return results || number;
}

module.exports = foobarqix;

function constructFooBarQixString(number, conditionalFunc) {
    let results = "";
    Object.keys(foobarqixObj).forEach(divisor => {
        if (conditionalFunc(number, divisor)) {
            results += foobarqixObj[divisor];
        }
    });
    return results;
}

function divisibleByDivisor(number, divisor) {
    return number % divisor === 0;
}

function containsFooBarNumber(number,fooBarNum) {
    return number.toString().includes(fooBarNum);
}
