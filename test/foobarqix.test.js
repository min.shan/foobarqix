const foobarqix = require('../src/foobarqix');
describe('foobarqix', () => {
    it('print number', () => {
        const result = foobarqix(1);
        expect(result).toBe(1);
    });
    it('print FooFoo when the input number contains 3 and divisible by 3', () => {
        const result = foobarqix(3);
        expect(result).toBe("FooFoo");
    });
    it('print BarBar when the input number contains 5 and divisible by 5', () => {
        const result = foobarqix(5);
        expect(result).toBe("BarBar");
    });
    it('print Foo when the input number is divisible by 3', () => {
        const result = foobarqix(6);
        expect(result).toBe("Foo");
    });
    it('print QixQix when the input number contains 7 and is divisible by 7', () => {
        const result = foobarqix(7);
        expect(result).toBe("QixQix");
    });
    it('print Foo when the input number is divisible by 5', () => {
        const result = foobarqix(10);
        expect(result).toBe("Bar");
    });
    it('print Foo when the input number contains 3', () => {
        const result = foobarqix(13);
        expect(result).toBe("Foo");
    });
    it('print Qix when the input number is divisible by 7', () => {
        const result = foobarqix(14);
        expect(result).toBe("Qix");
    });
    it('print FooBarBar when the input number divisible by 3 and 5, and contains 5 ', () => {
        const result = foobarqix(15);
        expect(result).toBe("FooBarBar");
    });
    it('print FooFooFoo when the input number contains 3 and divisible by 3', () => {
        const result = foobarqix(33);
        expect(result).toBe("FooFooFoo");
    });
})